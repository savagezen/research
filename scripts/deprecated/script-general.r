#!/usr/bin/R
# compute and graph correlation statistics for sleep study control control

# library
library(dplyr)
library(Hmisc)
library(corrplot)

raw_data <- read.delim("rawdata.tsv")
df <- as.matrix(as.data.frame(lapply(raw_data, as.numeric)))

sink("descriptive-stats.txt")
summary(raw_data)

sink("correlation-sleep_control.txt")
rcorr(df, type=c("pearson"))

res <- rcorr(df, type=c("pearson"))
corrplot(res$r, type="upper", order="hclust", p.mat = res$P, sig.level = 0.05, insig = "blank", main="Correlations at P = 0.05")
dev.copy(png, "plot-0500.png")
dev.off()
dev.off()

res <- rcorr(df, type=c("pearson"))
corrplot(res$r, type="upper", order="hclust", p.mat = res$P, sig.level = 0.01, insig = "blank", main="Correlations at P = 0.01")
dev.copy(png, "plot-0100.png")
dev.off()
dev.off()

res <- rcorr(df, type=c("pearson"))
corrplot(res$r, type="upper", order="hclust", p.mat = res$P, sig.level = 0.001, insig = "blank", main="Correlations at P = 0.001")
dev.copy(png, "plot-0010.png")
dev.off()
dev.off()

res <- rcorr(df, type=c("pearson"))
corrplot(res$r, type="upper", order="hclust", p.mat = res$P, sig.level = 0.0001, insig = "blank", main="Correlations at P = 0.0001")
dev.copy(png, "plot-0001.png")
dev.off()
dev.off()
