#!/usr/bin/R
# demo template for computing descriptives stats and correlations with plot.

# required pacakges
# - dplyr
# - psych (replaces: Hmisc)
# - corrplot

# library
library(dplyr)
library(psych)
library(corrplot)

# import raw data
# txt = read.txt
# csv = read.csv
# tsv = read.table
# xlsx = read.xl, requires package readxl
raw_data <- read.delim("raw_data-test.txt")

# convert data to numerical matrix
mtcars <- as.matrix(as.data.frame(lapply(raw_data, as.numeric)))

# descriptive stats
sink("descriptive_stats-test.txt")	# set output file
describe(mtcars)			# compute and write
sink()					# unset output file

# correlation and p-values
cor_results <- corr.test(mtcars)		# initial computation
alpha <- 0.05					# set threshold
cor_matrix <- cor_results$r		
p_matrix <- cor_results$p
cor_matrix[p_matrix >= alpha] <- NA		# replace non-significant values with NA
sink("correlation-test-p050.txt")
print(cor_matrix)

# plot
png("plot-test.png", width = 1080, height = 1080, units = "px")						# set output file
corrplot(cor_matrix, na.label = "NA", addCoef.col = "white")	# generate image
dev.off()							# close png device
