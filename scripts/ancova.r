#!/usr/bin/R
# sample for testing covariance

# library
# install.packages("dplyr")
library("dplyr")

# import raw data from .txt
raw_data <- read.delim("raw_data-test.txt")

# convert data to numerical matrix
df <- as.matrix(as.data.frame(lapply(raw_data, as.numeric)))

# fit ANCOVA model
ancova_model <- lm(output ~ input.1 + input.2, data=raw_data)

# view summary of model
summary(ancova_model)

# in results if p < 0.05, then that input 
# functions on the output independent of the other input

